package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main extends Application {

    private  ObservableList<String> tableItems;
    private TableView tableView;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Server");
        Scene scene1 = new Scene(new VBox(), 300, 275);
        tableView = new TableView();
        tableItems = FXCollections.observableArrayList ();


        ServerModel serverModel = new ServerModel();
        serverModel.SetPort(9090, tableItems);
        (serverModel).start();




        TextField textField = new TextField();
        Button sendButton = new Button();
        sendButton.setText("Send message");
        sendButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                try {
                    //serverModel.SendMessage(textField.getText());
                    serverModel.SendMessage("TEST");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });


        ////////////////////////////////////////////////////
        ((VBox)scene1.getRoot()).getChildren().addAll(tableView, textField, sendButton);
        primaryStage.setScene(scene1);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
