package sample;

import javafx.collections.ObservableList;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Created by Jan on 09.05.2017.
 */
public class ServerModel extends Thread {

    private ServerSocket serverSocket;
    private Socket socket;
    private boolean running;
    private int portNumber;
    private ObservableList<String> messagelist;
    private ArrayList<Socket> sockets;


    public void SetPort(int port, ObservableList<String> mList){
        portNumber = port;
        messagelist = mList;
    }

    @Override
    public void run(){
        try {
            running = true;
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void startServer() throws IOException{
        System.out.println("Starting server");
        serverSocket = new ServerSocket((int)4445);
        System.out.println(serverSocket.toString());
        sockets = new ArrayList<Socket>();
        socket = serverSocket.accept();
        System.out.println(socket.toString());
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        InputStream is;
        InputStreamReader isr;
        BufferedReader br;

        try {


            while (running) {

                try {
                    is = socket.getInputStream();
                    isr = new InputStreamReader(is);
                    br = new BufferedReader(isr);
                    String message = br.readLine();
                    System.out.println("Message received from client is "+message);
                } finally {
                    //socket.close();
                }

            }
        }
        finally {
            //listener.close();
        }
        System.out.println("End of loop");
    }




    public void SendMessage(String message) throws  IOException{
        System.out.println("Wysyłam");
        System.out.println(socket.toString());
        try {
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            bw.write(message);
            System.out.println("Message sent to the client is "+message);
            bw.flush();
        } finally {
            //socket.close();
        }
        System.out.println("Wysłano");

/*
        for(Socket s : sockets) {
            System.out.println(socket);
            try {
                PrintWriter out = new PrintWriter(s.getOutputStream());
                out.println(message);
                messagelist.add(message);
            } finally {
                //socket.close();
            }
        }
        */
    }

    public void StopServer() throws IOException {
        running = false;
        socket.close();
    }

}
