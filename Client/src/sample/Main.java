package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;

public class Main extends Application {

    private ObservableList<String> tableItems;
    private TableView tableView;

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Client");
        Scene scene1 = new Scene(new VBox(), 300, 275);
        tableView = new TableView();
        tableItems = FXCollections.observableArrayList ();

        ClientModel clientModel = new ClientModel();
        clientModel.SetClient(4445, "10.22.107.99",tableItems);
        (clientModel).start();




        TextField textField = new TextField();
        Button sendButton = new Button();
        sendButton.setText("Send message");
        sendButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                clientModel.SendMessage(textField.getText());
            }
        });


        ////////////////////////////////////////////////////
        ((VBox)scene1.getRoot()).getChildren().addAll(tableView, textField, sendButton);
        primaryStage.setScene(scene1);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}

