package sample;

import javafx.collections.ObservableList;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jan on 09.05.2017.
 */
public class ClientModel extends Thread {

    private ServerSocket listener;
    private Socket socket;
    private boolean running;
    private int portNumber;
    private String addrNumber;
    ObservableList<String> messageList;

    public void SetClient(int port, String addr, ObservableList<String> messList){
        portNumber = port;
        addrNumber = addr;
        messageList = messList;
    }


    public void run(){
        try {
            running = true;
            startCient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void startCient() throws IOException {
        String serverAddress = addrNumber;
        socket = new Socket(serverAddress, portNumber);
        System.out.println(socket.toString());
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        SendMessage("New client: "+dateFormat.format(date));
        while(running) {
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String answer = input.readLine();
            //JOptionPane.showMessageDialog(null, answer);
            if(answer!=null) {
                messageList.add(answer);
                System.out.println(answer);
            }
        }
    }

    public void SendMessage(String message){
        OutputStream os = null;
        try {
            os = socket.getOutputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        OutputStreamWriter osw = new OutputStreamWriter(os);
        BufferedWriter bw = new BufferedWriter(osw);

        try {
            bw.write(message);
            messageList.add(message);
            System.out.println("Wysłano na serwer: "+message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
