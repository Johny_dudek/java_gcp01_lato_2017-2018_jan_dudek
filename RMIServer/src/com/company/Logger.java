package com.company;

/**
 * Created by Jan on 3/14/2017.
 */
public interface Logger  {
    void log(String status, Student student);
}
