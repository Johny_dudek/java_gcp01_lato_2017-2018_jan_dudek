package com.company;

/**
 * Created by Jan on 3/15/2017.
 */
public class ConsoleLogger implements Logger, StudentEvent {
    @Override
    public void log(String status, Student student) {
        System.out.println(status+student.getFirstName()+" "+student.getLastName()+" "+student.getMark());
    }

    @Override
    public void eventHappened() {
        log("Event occured", new Student());
    }
}
