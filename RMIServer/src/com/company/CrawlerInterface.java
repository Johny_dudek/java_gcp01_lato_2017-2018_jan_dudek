package com.company;

import java.io.IOException;
import java.net.URL;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 * Created by Jan on 16.05.2017.
 */
public interface CrawlerInterface extends Remote {

    //public Crawler(Logger[] loggers) throws RemoteException;
    //public void setUrl(URL url);
    public void run() throws IOException, InterruptedException;
    //List<Student> extractStudents(Crawler.OrderMode mode );
    //double extractMark( Crawler.ExtremumMode mode );
    //int extractAge( Crawler.ExtremumMode mode );
}
