package com.company;



import javax.naming.NamingException;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Main {

    public static void main(String[] args) throws IOException, NamingException, RemoteException, InterruptedException, AlreadyBoundException {


        int port = 5070;

        String name = "rmi://" + port + "/CrawlerServer";
        Registry registry = LocateRegistry.createRegistry( port );

        final Logger[] loggers = new Logger[]
                {
                        new ConsoleLogger()
                };
        System.out.println("Ready");
        try
        {
            Crawler crawler = new Crawler(loggers);
            registry.bind( name, crawler );
            //crawler.setUrl(new URL("http://home.agh.edu.pl/~ggorecki/IS_Java/students.txt"));
            System.out.println("Binded -> Loop");
            while(true) {
                //infinite loop
            }

        }
        finally
        {
            UnicastRemoteObject.unexportObject( registry, true ); // zwalnianie rejestru
            System.out.println( "Server stopped." ); // komunikat zakonczenia
        }



/*
        Crawler crawler = new Crawler(loggers);
        crawler.setUrl(new URL("http://home.agh.edu.pl/~ggorecki/IS_Java/students.txt"));
        try {
            crawler.run();      //////////////////////RUN//////////////
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
*/

    }
}
