package com.company;



import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class Crawler extends UnicastRemoteObject implements CrawlerInterface {

    public enum OrderMode
    {
        MARK,
        FIRST_NAME,
        LAST_NAME,
        AGE;
    }
    public enum ExtremumMode
    {
        MAX,
        MIN
    }

    private Logger[] loggers;
    private URL url;
    private File file;
    private List<Student> students;
    private List<Student> previousStudents;
    private List<Student> addedStudents;
    private List<Student> deletedStudents;

    public Crawler(Logger[] loggers) throws RemoteException {
        //super();
        this.loggers = loggers;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public void run() throws IOException, InterruptedException {
        url = new URL("http://home.agh.edu.pl/~ggorecki/IS_Java/students.txt");
        System.out.println("Crawler started");
        addedStudents = new ArrayList<Student>();
        deletedStudents = new ArrayList<Student>();

        file = new File("students.txt");
        //FileUtils.copyURLToFile(url, file);
        List<Student> previousStudents = StudentsParser.parse(new File("students.txt"));
        do {
            //FileUtils.copyURLToFile(url, file);
            students = StudentsParser.parse(new File("students.txt"));

            for(Student s : students){
                if(!previousStudents.contains(s)){
                    addedStudents.add(s);
                    for(Logger el : loggers){
                        el.log("Dodano studenta: ", s);
                    }
                }
            }

            for(Student s : previousStudents){
                if(!students.contains(s)){
                    deletedStudents.add(s);
                    for(Logger el : loggers){
                        el.log("Usunięto studenta: ", s);
                    }
                }
            }

            if(addedStudents.size()>0 || deletedStudents.size()>0||true){
                System.out.println("Aktualna lista studentów:");
                for(Student s:extractStudents(OrderMode.LAST_NAME)){
                    loggers[0].log("",s);
                }
            }

            if(deletedStudents.size()==0&&addedStudents.size()==0){
                //wywłac zdarzenie "Brak zmian"
            }

            previousStudents = students;
            Thread.sleep(10000);
        } while (true);
    }


    List<Student> extractStudents( OrderMode mode ){
        Comparator<Student> tmp;
        switch(mode){
            case MARK: tmp = (Student s1, Student s2)->Double.compare(s1.getMark(),s2.getMark());
                break;
            case FIRST_NAME: tmp = (Student s1, Student s2)->s1.getFirstName().compareTo(s2.getFirstName());
                break;
            case LAST_NAME: tmp = (Student s1, Student s2)->s1.getLastName().compareTo(s2.getLastName());
                break;
            case AGE: tmp = (Student s1, Student s2)->Integer.compare(s1.getAge(),s2.getAge());
            break;
            default: tmp = (Student s1, Student s2)->s1.getLastName().compareTo(s2.getLastName());
        }
        Collections.sort(students,tmp);
        return students;
    }

    double extractMark( ExtremumMode mode ){
        double value;
        if(mode == ExtremumMode.MAX) {
            value = 0;
            for (Student s : students) {
                if (s.getMark() > value)
                    value = s.getMark();
            }
            return value;
        }else{
            value = 5;
            for (Student s : students) {
                if (s.getMark() < value)
                    value = s.getMark();
            }
            return  value;
        }
    }

    int extractAge( ExtremumMode mode ){
        int value;
        if(mode == ExtremumMode.MAX) {
            value = 0;
            for (Student s : students) {
                if (s.getAge() > value)
                    value = s.getAge();
            }
            return value;
        }else{
            value = 500;
            for (Student s : students) {
                if (s.getAge() < value)
                    value = s.getAge();
            }
            return  value;
        }
    }


}