package com.company;

import javax.naming.NamingException;
import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main {

    public static void main(String[] args)  throws NamingException, RemoteException, NotBoundException {
	// write your code here
        String host = "localhost";
        int port = 5070;

        String name = "rmi://" + port + "/CrawlerServer";

        Registry registry = LocateRegistry.getRegistry( host, port );
        CrawlerInterface crawler = (CrawlerInterface) registry.lookup( name );

        try {
            //crawler.setUrl(new URL("http://home.agh.edu.pl/~ggorecki/IS_Java/students.txt"));
            crawler.run();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }
}
